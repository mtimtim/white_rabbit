<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        //TODO implement this!
        // Maria :-)
        //delete all the non letter characters from the input file
        return strtolower(preg_replace("/[^A-Za-z]/", "", file_get_contents($filePath)));
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        //TODO implement this!
        //count the occurrences
        $occurrences = array(
                'a' => 0,
                'b' => 0,
                'c' => 0,
                'd' => 0,
                'e' => 0,
                'f' => 0,
                'g' => 0,
                'h' => 0,
                'i' => 0,
                'j' => 0,
                'k' => 0,
                'l' => 0,
                'm' => 0,
                'n' => 0,
                'o' => 0,
                'p' => 0,
                'q' => 0,
                'r' => 0,
                's' => 0,
                't' => 0,
                'u' => 0,
                'v' => 0,
                'w' => 0,
                'x' => 0,
                'y' => 0,
                'z' => 0
                );

        for( $i = 0; $i < strlen($parsedFile); $i++ ) {
                    $char = substr($parsedFile, $i, 1 );
                    $occurrences[$char]++;
                    }


        //sort the array
        asort($occurrences);

        //find the median position
        $medianpos = ceil(count($occurrences)/2);

        //get the letter at that position
        $medianletter = array_keys($occurrences)[$medianpos];

        //return the letter
        return $medianletter;
    }
}
