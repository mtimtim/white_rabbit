<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){
      $arr = array(
            '1'   => 0,
            '2'   => 0,
            '5'   => 0,
            '10'  => 0,
            '20'  => 0,
            '50'  => 0,
            '100' => 0
        );

    // in the worst case (any number, doesn't matter how large, ending in 88), it will max run 7 steps
    $rest = $amount;
    for ($i = count($arr) - 1; $i >= 0; $i--){
      if ($rest >= array_keys($arr)[$i]){
        $arr[array_keys($arr)[$i]] = intval($rest/array_keys($arr)[$i]);
        $rest = $rest % array_keys($arr)[$i];
       }
       if (!$rest) return $arr; // if I already have all the coins needed, no need for further comparation, return the result
    }
    return $arr;
    }
}
